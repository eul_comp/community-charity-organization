<?php
$servername = "localhost:3313";
$username = "root";
$password = "";
$database = "community";

// Create connection
$connection = new mysqli($servername, $username, $password, $database);


$D_name = "";
$D_phone= "";
$D_password = "";
$NationalID = "";
$LocationID = "";

$errorMessage = "";
$successMessage = "";

if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
    $D_name = $_POST["Donatorname"];
    $D_phone = $_POST["Donatorphone"];
    $D_password = $_POST["Donatorpassword"];
    $NationalID = $_POST["national"];
    $LocationID = $_POST["location"];

    do {
        if ( empty($D_name) || empty($D_phone) || empty($D_password) || empty($NationalID) || empty($LocationID) ) {
            $errorMessage = "All the fields are required";
            break;
        }

        // add new client to database
        $sql =  "INSERT INTO donator  (D_name, D_phone,D_password, NationalID, LocationID) " .
                "VALUES ('$D_name', '$D_phone','$D_password', '$NationalID', '$LocationID')";
        $result = $connection->query($sql);

        if (!$result) {
            $errorMessage = "Invalid query: " . $connection->error;
            break;
        }


     $D_name = "";
     $D_phone= "";
     $D_password = "";
     $NationalID = "";
     $LocationID = "";
        


        $successMessage = "Client added correctly";

        header("location: /charity/admin.html");
        exit;

    } while (false);
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Donator</title>
    <link href= "styles.css" rel="stylesheet" type= "text/css">
</head>
<body>
    <div class="container my-5">
        <h2>Register For Donations</h2>

      

        <form action=" " method="post">
            <fieldset>
            <legend>Personal information:</legend>
            <div class="row mb-3">
                <label class="col-sm-3 col-form-label">Donator name:</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="Donatorname" placeholder="Donator name" value="<?php echo $D_name; ?>">
                </div>
            </div>
            <div class="row mb-3">
                <label class="col-sm-3 col-form-label">D phone:</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="Donatorphone"  placeholder="Donator phone" value="<?php echo $D_phone; ?>">
                </div>
            </div>
            <div class="row mb-3">
                <label class="col-sm-3 col-form-label">D password:</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="Donatorpassword" placeholder="D password"  value="<?php echo $D_password; ?>">
                </div>
            </div>
            <div class="row mb-3">
                <label class="col-sm-3 col-form-label">National:</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="national" placeholder="National ID"  value="<?php echo $NationalID; ?>">
                </div>
            </div>
            <div class="row mb-3">
                <label class="col-sm-3 col-form-label">Location:</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="location"  placeholder="Location ID" value="<?php echo $LocationID; ?>">
                </div>
            </div>


            

            <div class="row bttn">
                <div class="colm left">
                    <button type="submit" class=" button button1">Submit</button>
                </div>
                <div class=" colm right">
                    <a class=" button3" href="/charity/admin.html" role="button">Cancel</a>
                </div>
            </div>
        </fieldset>
        </form>
    </div>
</body>
</html>