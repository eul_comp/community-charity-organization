<?php
$servername = "localhost:3313";
$username = "root";
$password = "";
$database = "community";

// Create connection
$connection = new mysqli($servername, $username, $password, $database);


$id="";
$EID= "";
$E_Name= "";
$E_Password = "";
$E_Phone = "";
$E_Task= "";

$errorMessage = "";
$successMessage = "";

if ( $_SERVER['REQUEST_METHOD'] == 'GET' ) {
    // GET method: Show the data of the client

    if ( !isset($_GET["id"]) ) {
        header("location: /charity/index.php");
        exit;
    }

    $id = $_GET["id"];

    // read the row of the selected client from database table
    $sql = "SELECT * FROM charity_emp WHERE id=$id";
    $result = $connection->query($sql);
    $row = $result->fetch_assoc();

    if (!$row) {
        header("location: /charity/index.php");
        exit;
    }

    $EID= $row["EID"];
    $E_Name = $row["E_Name"];
    $E_Password = $row["E_Password"];
    $E_Phone = $row["E_Phone"];
    $E_Task = $row["E_Task"];

}
else {
    // POST method: Update the data of the client
    $id= $_POST["id"];
    $EID = $_POST["eid"];
    $E_Name = $_POST["name"];
    $E_Password = $_POST["passd"];
    $E_Phone = $_POST["phone"];
    $E_Task = $_POST["task"];

    do {
        if ( empty($id) ||empty($EID) || empty($E_Name) || empty($E_Password) || empty($E_Phone) || empty($E_Task)  ) {
            $errorMessage = "All the fields are required";
            break;
        }

        $sql = "UPDATE `charity_emp` 
        SET  EID= '$EID', E_Name = '$E_Name',E_Password ='$E_Password',E_Phone = '$E_Phone',E_Task = '$E_Task'
         WHERE id = $id";

        $result = $connection->query($sql);

        if (!$result) {
            $errorMessage = "Invalid query: " . $connection->error;
            break;
        }

        $successMessage = "Client updated correctly";

        header("location: /charity/update_employee.php");
        exit;

    } while (false);
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
    
    <title>Needy People</title>
    
</head>
<body>
    <div class="container my-5">
        <h2>Employee Information</h2>

       

        <form action=" " method="post">
        <fieldset>
            <legend>Employee Personal information:</legend>
            <div class="row ">
            <input type="hidden" name="id" value="<?php echo $id; ?>">
            <label class=" left">Employee ID:</label>
                <div class="colm1 right">
                    <input type="text" name="eid" placeholder="National Id" value="<?php echo $EID; ?>" >
                </div>
            </div>
            <div class="row ">
                <label class="colm left">Employee Name:</label>
                <div class="colm right">
                    <input type="text"  name="name"  placeholder="First Name" value="<?php echo $E_Name; ?>" >
                </div>
            </div>
            <div class="row">
                <label class="colm left">Password:</label>
                <div class="colm right">
                    <input type="text"  name="passd" placeholder="Password" value="<?php echo $E_Password; ?>" >
                </div>
            </div>
            <div class="row ">
                <label class="colm left">Phone Number:</label>
                <div class="colm right">
                    <input type="text"  name="phone" placeholder="0*****"  value="<?php echo $E_Phone; ?>" >
                </div>
            </div>
            <div class="row ">
                <label class="colm left">Employee Task:</label>
                <div class="colm right">
                    <input type="text"  name="task"  placeholder="Task"  value="<?php echo $E_Task; ?>">
                </div>
            </div>


            

            <div class="row bttn">
                <div class="colm left">
                    <button type="submit" class=" button button1">Submit</button>
                </div>
                <div class=" colm right">
                    <a class=" button3" href="/charity/admin.html" role="button">Cancel</a>
                </div>
            </div>
        

            
            </fieldset>
        </form>
    </div>
</body>
</html>