<?php
$servername = "localhost:3313";
$username = "root";
$password = "";
$database = "community";

// Create connection
$connection = new mysqli($servername, $username, $password, $database);


$NationalID = "";
$FName = "";
$LName = "";
$NPhone = "";
$LocationID = "";

$errorMessage = "";
$successMessage = "";

if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
    $NationalID = $_POST["national"];
    $FName = $_POST["firstname"];
    $LName = $_POST["lastname"];
    $NPhone = $_POST["phone"];
    $LocationID = $_POST["location"];

    do {
        if ( empty($NationalID) || empty($FName) || empty($LName) || empty($NPhone) || empty($LocationID) ) {
            $errorMessage = "All the fields are required";
            break;
        }

        // add new client to database
        $sql =  "INSERT INTO needy_people (NationalID, FName,LName, NPhone, LocationID) " .
                "VALUES ('$NationalID', '$FName', '$LName','$NPhone', '$LocationID')";
        $result = $connection->query($sql);

        if (!$result) {
            $errorMessage = "Invalid query: " . $connection->error;
            break;
        }

    
        $NationalID = "";
        $FName = "";
        $LName = "";
        $NPhone = "";
        $LocationID = "";
        


        $successMessage = "Client added correctly";

        header("location: /crudnew/index.php");
        exit;

    } while (false);
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js"></script>
</head>
<body>
    <div class="container my-5">
        <h2>Register For Donations</h2>

        <?php
        if ( !empty($errorMessage) ) {
            echo "
            <div class='alert alert-warning alert-dismissible fade show' role='alert'>
                <strong>$errorMessage</strong>
                <button type='button' class='btn-close' data-bs-dismiss='alert' aria-label='Close'></button>
            </div>
            ";
        }
        ?>

        <form action=" " method="post">
            <fieldset>
            <legend>Personal information:</legend>
            <div class="row mb-3">
                <label class="col-sm-3 col-form-label">National ID:</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="national" placeholder="National Id" value="<?php echo $NationalID; ?>">
                </div>
            </div>
            <div class="row mb-3">
                <label class="col-sm-3 col-form-label">First Name:</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="firstname"  placeholder="First Name" value="<?php echo $FName; ?>">
                </div>
            </div>
            <div class="row mb-3">
                <label class="col-sm-3 col-form-label">Last Name:</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="lastname" placeholder="Last Name"  value="<?php echo $LName; ?>">
                </div>
            </div>
            <div class="row mb-3">
                <label class="col-sm-3 col-form-label">Phone Number:</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="phone" placeholder="0*****"  value="<?php echo $NPhone; ?>">
                </div>
            </div>
            <div class="row mb-3">
                <label class="col-sm-3 col-form-label">Location:</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="location"  placeholder="Location ID" value="<?php echo $LocationID; ?>">
                </div>
            </div>


            <?php
            if ( !empty($successMessage) ) {
                echo "
                <div class='row mb-3'>
                    <div class='offset-sm-3 col-sm-6'>
                        <div class='alert alert-success alert-dismissible fade show' role='alert'>
                            <strong>$successMessage</strong>
                            <button type='button' class='btn-close' data-bs-dismiss='alert' aria-label='Close'></button>
                        </div>
                    </div>
                </div>
                ";
            }
            ?>

            <div class="row mb-3">
                <div class="offset-sm-3 col-sm-3 d-grid">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                <div class="col-sm-3 d-grid">
                    <a class="btn btn-outline-primary" href="/crudnew/index.php" role="button">Cancel</a>
                </div>
            </div>
        </fieldset>
        </form>
    </div>
</body>
</html>