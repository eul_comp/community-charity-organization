<?php
$servername = "localhost:3313";
$username = "root";
$password = "";
$database = "community";

// Create connection
$connection = new mysqli($servername, $username, $password, $database);



$DID = "";
$D_name = "";
$D_phone = "";
$D_email = "";
$D_Type = "";



$errorMessage = "";
$successMessage = "";

if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
    $DID = $_POST["id"];
    $D_name = $_POST["name"];
    $D_phone = $_POST["phone"];
    $D_email = $_POST["email"];
    $D_Type = $_POST["type"];

    do {
        if ( empty($DID) || empty($D_name) || empty($D_phone) || empty($D_email) || empty($D_Type) ) {
            $errorMessage = "All the fields are required";
            break;
        }

        // add new client to database
        $sql =  "INSERT INTO donation ( DID, D_name, D_phone, D_email, D_Type ) " .
                "VALUES ('$DID', '$D_name', '$D_phone', '$D_email', '$D_Type')";
        $result = $connection->query($sql);

        if (!$result) {
            $errorMessage = "Invalid query: " . $connection->error;
            break;
        }

    
        
        $DID = "";
        $D_name = "";
        $D_phone = "";
        $D_email = "";
        $D_Type = "";


        $successMessage = "Client added correctly";

        header("location: /charity/index.html");
        exit;

    } while (false);
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <div class="container my-5">
        <h2>Register For Donations</h2>

        

        <form action=" " method="post">
            <fieldset>
            <legend>Personal information:</legend>
            <div class="row ">
            <input type="hidden" name="id" value="<?php echo $id; ?>">
            <div class="row mb-3">
                <label class="col-sm-3 col-form-label"> DID:</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="id" placeholder="Donation Id" value="<?php echo $DID; ?>">
                </div>
            </div>
            <div class="row mb-3">
                <label class="col-sm-3 col-form-label">D_name:</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="name"  placeholder="Donation name" value="<?php echo $D_name; ?>">
                </div>
            </div>
            <div class="row mb-3">
                <label class="col-sm-3 col-form-label">D_phone:</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="phone" placeholder="Donation phone" value="<?php echo $D_phone; ?>">
                </div>
            </div>
            <div class="row mb-3">
                <label class="col-sm-3 col-form-label">D_email:</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="email"  placeholder="Donation email" value="<?php echo $D_email; ?>">
                </div>
            </div>
            <div class="row mb-3">
                <label class="col-sm-3 col-form-label">D_Type:</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="type"  placeholder="Donation type" value="<?php echo $D_Type; ?>">
                </div>
            </div>
        

            
        

            <div class="row bttn">
                <div class="colm left">
                    <button type="submit" class=" button button1">Submit</button>
                </div>
                <div class=" colm right">
                    <a class=" button3" href="/charity/index.html" role="button">Cancel</a>
                </div>
            </div>
        </fieldset>
        </form>
    </div>
</body>
</html>