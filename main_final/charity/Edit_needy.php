<?php
$servername = "localhost:3313";
$username = "root";
$password = "";
$database = "community";

// Create connection
$connection = new mysqli($servername, $username, $password, $database);


$id = "";
$NationalID = "";
$FName = "";
$LName = "";
$NPhone = "";
$LocationID = "";

$errorMessage = "";
$successMessage = "";

if ( $_SERVER['REQUEST_METHOD'] == 'GET' ) {
    // GET method: Show the data of the client

    if ( !isset($_GET["id"]) ) {
        header("location: /charity/index.php");
        exit;
    }

    $id = $_GET["id"];

    // read the row of the selected client from database table
    $sql = "SELECT * FROM needy_people WHERE id=$id";
    $result = $connection->query($sql);
    $row = $result->fetch_assoc();

    if (!$row) {
        header("location: /charity/index.php");
        exit;
    }

    $NationalID= $row["NationalID"];
    $FName = $row["FName"];
    $LName = $row["LName"];
    $NPhone = $row["NPhone"];
    $LocationID = $row["LocationID"];
}
else {
    // POST method: Update the data of the client

    $id = $_POST["id"];
    $NationalID = $_POST["national"];
    $FName = $_POST["firstname"];
    $LName = $_POST["lastname"];
    $NPhone = $_POST["phone"];
    $LocationID = $_POST["location"];

    do {
        if ( empty($id) ||  empty($NationalID) || empty($FName) || empty($LName) || empty($NPhone) || empty($LocationID)  ) {
            $errorMessage = "All the fields are required";
            break;
        }

        $sql = "UPDATE `needy_people` 
        SET  NationalID= '$NationalID', FName = '$FName',LName ='$LName',NPhone = '$NPhone',LocationID = '$LocationID'
         WHERE id = $id";

        $result = $connection->query($sql);

        if (!$result) {
            $errorMessage = "Invalid query: " . $connection->error;
            break;
        }

        $successMessage = "Client updated correctly";

        header("location: /charity/update_needy.php");
        exit;

    } while (false);
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
    
    <title>Beneficiaries</title>
    
</head>
<body>
    <div class="container my-5">
        <h2>Beneficiary Information</h2>

       

        <form action=" " method="post">
        <fieldset>
            <legend>Personal information:</legend>
            <input type="hidden" name="id" value="<?php echo $id; ?>">
            <div class="row mb-3">
                <label class="col-sm-3 col-form-label">National ID:</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="national" value="<?php echo $NationalID; ?>">
                </div>
            </div>
            <div class="row mb-3">
                <label class="col-sm-3 col-form-label">First Name:</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="firstname" value="<?php echo $FName; ?>">
                </div>
            </div>
            <div class="row mb-3">
                <label class="col-sm-3 col-form-label">Last Name:</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="lastname" value="<?php echo $LName; ?>">
                </div>
            </div>
            <div class="row mb-3">
                <label class="col-sm-3 col-form-label">Phone Number:</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="phone" value="<?php echo $NPhone; ?>">
                </div>
            </div>
            <div class="row mb-3">
                <label class="col-sm-3 col-form-label">Location:</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="location" value="<?php echo $LocationID; ?>">
                </div>
            </div>

            <div class="row subButton">
                <div class="colm left">
                    <button type="submit" class="button ">update</button>
                </div>
                <div class="colm right">
                    <a class=" button " href="/charity/admin.html"><button class="button ">Cancel</button></a>
                </div>
            </div>
           

            
            </fieldset>
        </form>
    </div>
</body>
</html>