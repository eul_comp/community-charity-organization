<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Beneficiary</title>
    <link href= "styles.css" rel="stylesheet" type= "text/css">
</head>
<body>
    <div class="container my-5">
        <h2>List of Donors/Beneficiaries</h2>
       
        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Employee ID</th>
                    <th>Employee Name</th>
                    <th>Employee Phone</th>
                    <th>Employee Password</th>
                    <th>Employee Task</th>
                    
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $servername = "localhost:3313";
                $username = "root";
                $password = "";
                $database = "community";

                // Create connection
                $connection = new mysqli($servername, $username, $password, $database);
                // Check connection
                if ($connection->connect_error) {
                    die("Connection failed: " . $connection->connect_error);
                }

                // read all row from database table
                $sql = "SELECT * FROM charity_emp";
                $result = $connection->query($sql);

                if (!$result) {
                    die("Invalid query: " . $connection->error);
                }

                // read data of each row
                while($row = $result->fetch_assoc()) {
                    echo "
                    <tr>
                       
                        <td>$row[id]</td>
                        <td>$row[EID]</td>
                        <td>$row[E_Name]</td>
                        <td>$row[E_Phone]</td>
                        <td>$row[E_Password]</td>
                        <td>$row[E_Task]</td>
                        
                        <br />
                        <td>
                           
                            <a class='btn btn-danger btn-sm' href='/charity/delete_employee.php?id=$row[id]'>Delete</a>
                        </td>
                    </tr>
                    ";
                }

                ?>

                
            </tbody>
		</table>
    </div>
</body>
</html>
