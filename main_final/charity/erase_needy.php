<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BENEFICIARIES</title>
    <link href= "styles.css" rel="stylesheet" type= "text/css">
</head>
<body>
    <div class="container my-5">
        <h2>List of Beneficiaries</h2>
       
        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>National ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Phone Number</th>
                    <th>Location</th>
                    
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $servername = "localhost:3313";
                $username = "root";
                $password = "";
                $database = "community";

                // Create connection
                $connection = new mysqli($servername, $username, $password, $database);

                // Check connection
                if ($connection->connect_error) {
                    die("Connection failed: " . $connection->connect_error);
                }

                // read all row from database table
                $sql = "SELECT * FROM needy_people";
                $result = $connection->query($sql);

                if (!$result) {
                    die("Invalid query: " . $connection->error);
                }

                // read data of each row
                while($row = $result->fetch_assoc()) {
                    echo "
                    <tr>
                        <td>$row[id]</td>
                        <td>$row[NationalID]</td>
                        <td>$row[FName]</td>
                        <td>$row[LName]</td>
                        <td>$row[NPhone]</td>
                        <td>$row[LocationID]</td>
                        
                        
                        <br />
                        <td>
                           
                            <a class='btn btn-danger btn-sm' href='/charity/delete_needy.php?id=$row[id]'>Delete</a>
                        </td>
                    </tr>
                    ";
                }

                ?>

                
            </tbody>
		</table>
    </div>
</body>
</html>

