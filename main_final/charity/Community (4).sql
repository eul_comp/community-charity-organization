-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3313
-- Generation Time: Jun 15, 2022 at 01:45 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `community`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `userid` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(60) NOT NULL,
  `fullname` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`userid`, `username`, `password`, `fullname`) VALUES
(1, 'admin', 'admin', 'ngaibake'),
(2, 'ngaibake', 'freeman', 'nathan moyana');

-- --------------------------------------------------------

--
-- Table structure for table `charity_emp`
--

CREATE TABLE `charity_emp` (
  `id` int(11) NOT NULL,
  `EID` int(11) NOT NULL,
  `E_Name` tinytext NOT NULL,
  `E_Password` varchar(100) NOT NULL,
  `E_Phone` varchar(20) NOT NULL,
  `E_Task` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `charity_emp`
--

INSERT INTO `charity_emp` (`id`, `EID`, `E_Name`, `E_Password`, `E_Phone`, `E_Task`) VALUES
(1, 23, 'Fizzy Gee', '235647', '+87954678', 'driver'),
(2, 1234, 'nat moyana', '123', '09873562', 'cleaning'),
(3, 6473, 'mama mafia', '984743', '053385448748', 'cleaning and cooking');

-- --------------------------------------------------------

--
-- Table structure for table `donation`
--

CREATE TABLE `donation` (
  `id` int(11) NOT NULL,
  `DID` int(11) NOT NULL,
  `D_name` varchar(50) NOT NULL,
  `D_phone` varchar(50) NOT NULL,
  `D_email` varchar(100) NOT NULL,
  `D_Type` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `donation`
--

INSERT INTO `donation` (`id`, `DID`, `D_name`, `D_phone`, `D_email`, `D_Type`) VALUES
(1, 43, 'lawrence', '05338276162', 'law@gmail.com', 'money'),
(2, 43, 'lawrance', '2638827938', 'law@gmail.com', 'money'),
(3, 78, 'pombi', '88498738379', 'p@gmail.com', 'food'),
(4, 0, '[peter]', '[04476382728]', '[ptr@gmail.com]', '[clothes]');

-- --------------------------------------------------------

--
-- Table structure for table `donator`
--

CREATE TABLE `donator` (
  `id` int(11) NOT NULL,
  `D_name` varchar(20) NOT NULL,
  `D_phone` varchar(20) NOT NULL,
  `D_password` varchar(100) NOT NULL,
  `NationalID` varchar(20) NOT NULL,
  `LocationID` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `donator`
--

INSERT INTO `donator` (`id`, `D_name`, `D_phone`, `D_password`, `NationalID`, `LocationID`) VALUES
(2, 'Stuart Garry', '09865647388', '321456', '8372', 'popstone'),
(3, 'Stuart ginger', '098656473564', '1234567', '4367895', '0'),
(6, 'Giant man', '05534242617', 'wer2677', '09489292', 'lefkosa'),
(7, 'sj lawrrtyuu', '05534242617678', '98076', '5644', 'magusa'),
(9, 'First', '0986564738809', '9080', '0', '0'),
(12, 'Stuart Garry', '09864242617', '6758', 'GT6785', 'cxzf'),
(13, 'RS2356', '+560987453', '3242', 'DF453E56', 'gwanda');

-- --------------------------------------------------------

--
-- Table structure for table `needy_people`
--

CREATE TABLE `needy_people` (
  `id` int(100) NOT NULL,
  `NationalID` varchar(100) NOT NULL,
  `FName` varchar(50) NOT NULL,
  `LName` varchar(50) NOT NULL,
  `NPhone` varchar(50) NOT NULL,
  `LocationID` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `needy_people`
--

INSERT INTO `needy_people` (`id`, `NationalID`, `FName`, `LName`, `NPhone`, `LocationID`) VALUES
(1, 'FN43562w45', 'nathan', 'mombe', '0533987654', 'GAZ 123'),
(7, 'fh7654', 'jj', 'silver', '0123456709999', 'ny 1234'),
(12, 'rj6574', 'lionel', 'messi number10', '089755433344', 'nypd342'),
(16, 'FN43567278', 'jj', 'mbers', '0533897675', 'Grew');

-- --------------------------------------------------------

--
-- Table structure for table `type`
--

CREATE TABLE `type` (
  `TID` int(11) NOT NULL,
  `T_Name` tinytext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `volunteers`
--

CREATE TABLE `volunteers` (
  `id` int(20) NOT NULL,
  `National_ID` varchar(20) NOT NULL,
  `F_Name` varchar(100) NOT NULL,
  `L_Name` varchar(100) NOT NULL,
  `N_Phone` varchar(60) NOT NULL,
  `Location_ID` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `volunteers`
--

INSERT INTO `volunteers` (`id`, `National_ID`, `F_Name`, `L_Name`, `N_Phone`, `Location_ID`) VALUES
(1, 'FN435672', 'nathan', 'Evanz mwaruwaro', '01234567', 'GAZ 123'),
(2, 'FN435672', 'nathan', 'Evanz', '01234567', 'GAZ 123'),
(3, 'FT435672', 'ellie', 'Evanz', '01234567123', 'GAZ 12323'),
(4, 'GZ3456', 'Harry', 'Porter', '+786544234', 'Chikurubi'),
(6, 'JY 1627', 'mofassa ', 'kamari', '07467662773', 'gaza123'),
(7, '999', 'mponda', 'sugarcane', '99887', 'hj765');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`userid`);

--
-- Indexes for table `charity_emp`
--
ALTER TABLE `charity_emp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `donation`
--
ALTER TABLE `donation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `D_Type` (`D_Type`);

--
-- Indexes for table `donator`
--
ALTER TABLE `donator`
  ADD PRIMARY KEY (`id`),
  ADD KEY `NationaID` (`NationalID`,`LocationID`);

--
-- Indexes for table `needy_people`
--
ALTER TABLE `needy_people`
  ADD PRIMARY KEY (`id`),
  ADD KEY `LocationID` (`LocationID`);

--
-- Indexes for table `type`
--
ALTER TABLE `type`
  ADD PRIMARY KEY (`TID`);

--
-- Indexes for table `volunteers`
--
ALTER TABLE `volunteers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `userid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `charity_emp`
--
ALTER TABLE `charity_emp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `donation`
--
ALTER TABLE `donation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `donator`
--
ALTER TABLE `donator`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `needy_people`
--
ALTER TABLE `needy_people`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `type`
--
ALTER TABLE `type`
  MODIFY `TID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `volunteers`
--
ALTER TABLE `volunteers`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
