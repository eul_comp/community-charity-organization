<?php
$servername = "localhost";
$username = "root";
$password = "";
$database = "community";

// Create connection
$connection = new mysqli($servername, $username, $password, $database);


$National_ID = "";
$F_Name = "";
$L_Name = "";
$N_Phone = "";
$Location_ID = "";

$errorMessage = "";
$successMessage = "";

if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
    $National_ID = $_POST["national"];
    $F_Name = $_POST["firstname"];
    $L_Name = $_POST["lastname"];
    $N_Phone = $_POST["phone"];
    $Location_ID = $_POST["location"];

    do {
        if ( empty($National_ID) || empty($F_Name) || empty($L_Name) || empty($N_Phone) || empty($Location_ID) ) {
            $errorMessage = "All the fields are required";
            break;
        }

        // add new client to database
        $sql =  "INSERT INTO volunteers (National_ID, F_Name,L_Name, N_Phone, Location_ID) " .
                "VALUES ('$National_ID', '$F_Name', '$L_Name','$N_Phone', '$Location_ID')";
        $result = $connection->query($sql);

        if (!$result) {
            $errorMessage = "Invalid query: " . $connection->error;
            break;
        }

    
        $National_ID = "";
        $_FName = "";
        $L_Name = "";
        $N_Phone = "";
        $Location_ID = "";
        


        $successMessage = "Client added correctly";

        header("location: /charity/register.html");
        exit;

    } while (false);
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <div class="container my-5">
        <h2>Register For Donations</h2>

       

        <form action=" " method="post">
            <fieldset>
            <legend>Personal information:</legend>
            <div class="row mb-3">
                <label class="col-sm-3 col-form-label">National ID:</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="national" placeholder="National Id" value="<?php echo $National_ID; ?>">
                </div>
            </div>
            <div class="row mb-3">
                <label class="col-sm-3 col-form-label">First Name:</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="firstname"  placeholder="First Name" value="<?php echo $F_Name; ?>">
                </div>
            </div>
            <div class="row mb-3">
                <label class="col-sm-3 col-form-label">Last Name:</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="lastname" placeholder="Last Name"  value="<?php echo $L_Name; ?>">
                </div>
            </div>
            <div class="row mb-3">
                <label class="col-sm-3 col-form-label">Phone Number:</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="phone" placeholder="0*****"  value="<?php echo $N_Phone; ?>">
                </div>
            </div>
            <div class="row mb-3">
                <label class="col-sm-3 col-form-label">Location:</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="location"  placeholder="Location ID" value="<?php echo $Location_ID; ?>">
                </div>
            </div>


            

            <div class="row bttn">
                <div class="colm left">
                    <button type="submit" class=" button button1">Submit</button>
                </div>
                <div class=" colm right">
                    <a class=" button3" href="/latest/admin.html" role="button">Cancel</a>
                </div>
            </div>
        </fieldset>
        </form>
    </div>
</body>
</html>