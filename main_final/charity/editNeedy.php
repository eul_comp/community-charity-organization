<?php
$servername = "localhost:3313";
$username = "root";
$password = "";
$database = "community";

// Create connection
$connection = new mysqli($servername, $username, $password, $database);


$id = "";
$NationalID = "";
$FName = "";
$LName = "";
$NPhone = "";
$LocationID = "";

$errorMessage = "";
$successMessage = "";

if ( $_SERVER['REQUEST_METHOD'] == 'GET' ) {
    // GET method: Show the data of the client

    if ( !isset($_GET["id"]) ) {
        header("location: /charity/index.php");
        exit;
    }

    $id = $_GET["id"];

    // read the row of the selected client from database table
    $sql = "SELECT * FROM needy_people WHERE id=$id";
    $result = $connection->query($sql);
    $row = $result->fetch_assoc();

    if (!$row) {
        header("location: /charity/index.php");
        exit;
    }

    $NationalID= $row["NationalID"];
    $FName = $row["FName"];
    $LName = $row["LName"];
    $NPhone = $row["NPhone"];
    $LocationID = $row["LocationID"];
}
else {
    // POST method: Update the data of the client

    $id = $_POST["id"];
    $NationalID = $_POST["national"];
    $FName = $_POST["firstname"];
    $LName = $_POST["lastname"];
    $NPhone = $_POST["phone"];
    $LocationID = $_POST["location"];

    do {
        if ( empty($id) ||  empty($NationalID) || empty($FName) || empty($LName) || empty($NPhone) || empty($LocationID)  ) {
            $errorMessage = "All the fields are required";
            break;
        }

        $sql = "UPDATE `needy_people` 
        SET  NationalID= '$NationalID', FName = '$FName',LName ='$LName',NPhone = '$NPhone',LocationID = '$LocationID'
         WHERE id = $id";

        $result = $connection->query($sql);

        if (!$result) {
            $errorMessage = "Invalid query: " . $connection->error;
            break;
        }

        $successMessage = "Client updated correctly";

        header("location: /charity/index.php");
        exit;

    } while (false);
}
?>

