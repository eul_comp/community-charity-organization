<?php
$servername = "localhost";
$username = "root";
$password = "";
$database = "community";

// Create connection
$connection = new mysqli($servername, $username, $password, $database);


$id = "";
$D_name = "";
$D_phone= "";
$D_password = "";
$NationalID = "";
$LocationID = "";

$errorMessage = "";
$successMessage = "";

if ( $_SERVER['REQUEST_METHOD'] == 'GET' ) {
    // GET method: Show the data of the client

    if ( !isset($_GET["id"]) ) {
        header("location: /latest/index.php");
        exit;
    }

    $id = $_GET["id"];

    // read the row of the selected client from database table
    $sql = "SELECT * FROM donator WHERE id=$id";
    $result = $connection->query($sql);
    $row = $result->fetch_assoc();

    if (!$row) {
        header("location: /latest/update_donater.php");
        exit;
    }

    $D_name = $row["D_name"];
    $D_phone = $row["D_phone"];
    $D_password = $row["D_password"];
    $NationalID = $row["NationalID"];
    $LocationID = $row["LocationID"];

}
else {
    // POST method: Update the data of the client

    $id = $_POST["id"];
    $D_name = $_POST["Donator"];
    $D_phone = $_POST["phone"];
    $D_password = $_POST["password"];
    $NationalID = $_POST["National"];
    $LocationID = $_POST["location"];


    do {
        if ( empty($id) ||  empty($D_name) || empty($D_phone) || empty($D_password) || empty($NationalID) || empty($LocationID)  ) {
            $errorMessage = "All the fields are required";
            break;
        }

        $sql = "UPDATE `donator` 
        SET  D_name= '$D_name', D_phone = '$D_phone',D_password ='$D_password',NationalID = '$NationalID',LocationID = '$LocationID'
         WHERE id = $id";

        $result = $connection->query($sql);

        if (!$result) {
            $errorMessage = "Invalid query: " . $connection->error;
            break;
        }

        $successMessage = "Client updated correctly";

        header("location: /latest/update_donater.php");
        exit;

    } while (false);
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
    
    <title>Contributors</title>
    
</head>
<body>
    <div class="container my-5">
        <h2>Donor Information</h2>

       

        <form action=" " method="post">
        <fieldset>
            <legend>Personal information:</legend>
            <input type="hidden" name="id" value="<?php echo $id; ?>">
            <div class="row mb-3">
                <label class="col-sm-3 col-form-label">Donor Name:</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="Donator" value="<?php echo $D_name; ?>">
                </div>
            </div>
            <div class="row mb-3">
                <label class="col-sm-3 col-form-label">Phone number:</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="phone" value="<?php echo $D_phone; ?>">
                </div>
            </div>
            <div class="row mb-3">
                <label class="col-sm-3 col-form-label">Password:</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="password" value="<?php echo $D_password; ?>">
                </div>
            </div>
            <div class="row mb-3">
                <label class="col-sm-3 col-form-label">National ID:</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="National" value="<?php echo $NationalID; ?>">
                </div>
            </div>
            <div class="row mb-3">
                <label class="col-sm-3 col-form-label">Location:</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="location" value="<?php echo $LocationID; ?>">
                </div>
            </div>

            <div class="row subButton">
                <div class="colm left">
                    <button type="submit" class="button ">update</button>
                </div>
                <div class="colm right">
                    <a class=" button " href="/latest/admin.html"><button class="button ">Cancel</button></a>
                </div>
            </div>
           

            
            </fieldset>
        </form>
    </div>
</body>
</html>