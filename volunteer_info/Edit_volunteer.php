<?php
$servername = "localhost:3313";
$username = "root";
$password = "";
$database = "community";

// Create connection
$connection = new mysqli($servername, $username, $password, $database);


$id = "";
$National_ID = "";
$F_Name = "";
$L_Name = "";
$N_Phone = "";
$Location_ID = "";

$errorMessage = "";
$successMessage = "";

if ( $_SERVER['REQUEST_METHOD'] == 'GET' ) {
    // GET method: Show the data of the client

    if ( !isset($_GET["id"]) ) {
        header("location: /3313/index.php");
        exit;
    }

    $id = $_GET["id"];

    // read the row of the selected client from database table
    $sql = "SELECT * FROM volunteers WHERE id=$id";
    $result = $connection->query($sql);
    $row = $result->fetch_assoc();

    if (!$row) {
        header("location: /latest/index.php");
        exit;
    }

    $National_ID= $row["National_ID"];
    $F_Name = $row["F_Name"];
    $L_Name = $row["L_Name"];
    $N_Phone = $row["N_Phone"];
    $Location_ID = $row["Location_ID"];
}
else {
    // POST method: Update the data of the client

    $id = $_POST["id"];
    $National_ID = $_POST["national"];
    $F_Name = $_POST["firstname"];
    $L_Name = $_POST["lastname"];
    $N_Phone = $_POST["phone"];
    $Location_ID = $_POST["location"];

    do {
        if ( empty($id) ||  empty($National_ID) || empty($F_Name) || empty($L_Name) || empty($N_Phone) || empty($Location_ID)  ) {
            $errorMessage = "All the fields are required";
            break;
        }

        $sql = "UPDATE `volunteers` 
        SET  National_ID= '$National_ID', F_Name = '$F_Name',L_Name ='$L_Name',N_Phone = '$N_Phone',Location_ID = '$Location_ID'
         WHERE id = $id";

        $result = $connection->query($sql);

        if (!$result) {
            $errorMessage = "Invalid query: " . $connection->error;
            break;
        }

        $successMessage = "Client updated correctly";

        header("location: /charity/update_volunteer.php");
        exit;

    } while (false);
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
    
    <title>Charity Work Volunteers</title>
    
</head>
<body>
    <div class="container my-5">
        <h2>Volunteer Information</h2>

       

        <form action=" " method="post">
        <fieldset>
            <legend>Personal information:</legend>
            <input type="hidden" name="id" value="<?php echo $id; ?>">
            <div class="row mb-3">
                <label class="col-sm-3 col-form-label">National ID:</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="national" value="<?php echo $National_ID; ?>">
                </div>
            </div>
            <div class="row mb-3">
                <label class="col-sm-3 col-form-label">First Name:</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="firstname" value="<?php echo $F_Name; ?>">
                </div>
            </div>
            <div class="row mb-3">
                <label class="col-sm-3 col-form-label">Last Name:</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="lastname" value="<?php echo $L_Name; ?>">
                </div>
            </div>
            <div class="row mb-3">
                <label class="col-sm-3 col-form-label">Phone Number:</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="phone" value="<?php echo $N_Phone; ?>">
                </div>
            </div>
            <div class="row mb-3">
                <label class="col-sm-3 col-form-label">Location:</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="location" value="<?php echo $Location_ID; ?>">
                </div>
            </div>

            <div class="row subButton">
                <div class="colm left">
                    <button type="submit" class="button ">update</button>
                </div>
                <div class="colm right">
                    <a class=" button " href="/charity/index.php"><button class="button ">Cancel</button></a>
                </div>
            </div>
           

            
            </fieldset>
        </form>
    </div>
</body>
</html>